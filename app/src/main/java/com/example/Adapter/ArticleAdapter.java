package com.example.Adapter;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;

public class ArticleAdapter implements Parcelable {

    public static final String TAG = ArticleAdapter.class.getSimpleName() ;
    private String id;
    private String name;

    private List<Integer> timeFrame=new ArrayList<>();;

    private String description;

    private int year;
    private String brand;
    private String[] technicalDetails;
    private String[] pictures;
    private String thumbnail;
    private List<String> categories=new ArrayList<>();;
    public ArticleAdapter(String id) {
        this.id = id;
        this.year=-1;
    }

    public ArticleAdapter(String id, String name, List<String> categories, String description, List<Integer> timeFrame) {
        this.id = id;
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year=0;
    }

    public static final Creator<ArticleAdapter> CREATOR = new Creator<ArticleAdapter>() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public ArticleAdapter createFromParcel(Parcel in) {
            return new ArticleAdapter(in);
        }

        @Override
        public ArticleAdapter[] newArray(int size) {


            return new ArticleAdapter[size];
        }
    };

    public String getId() {

        return id; }
    public String getName() {

        return name; }

    public List<String> getCategories() {

        return categories;
    }
    public String getCategoriesToString() {
        if(categories!=null){
            StringBuilder result=new StringBuilder();
            for (String category : categories) {
                result.append(category+"-");
            }
            result.delete(result.length()-1,result.length());
            return result.toString();
        }
        return null;
    }
    public String getDescription() {

        return description;

    }


    public int getYear() {

        return year;
    }
    public String getBrand() {

        return brand;
    }


    public String getThumbnail() {

        return thumbnail;

    }

    public void setId(String id) {

        this.id = id;

    }
    public void setName(String name) {

        this.name = name;

    }

    public void setCategories(List<String> categories) {

        this.categories = categories;


    }
    public void setDescription(String description) {

        this.description = description;

    }


    public void setTimeFrame(List<Integer> timeFrame) {

        this.timeFrame = timeFrame;

    }

    public void setYear(int year) {

        this.year = year;

    }

    public void setBrand(String brand) {

        this.brand = brand;
    }




    public void setThumbnail(String thumbnail) {

        this.thumbnail = thumbnail;

    }
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {


        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @SuppressLint("NewApi")
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeList(categories);
        dest.writeString(description);
        dest.writeList(timeFrame);
        dest.writeInt(year);
        dest.writeString(brand);
        dest.writeStringArray(technicalDetails);
        dest.writeStringArray(pictures);
        dest.writeString(thumbnail);
    }
    @RequiresApi(api = Build.VERSION_CODES.Q)
    private ArticleAdapter(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();

        this.description = in.readString();
        in.readList(this.timeFrame,Integer.class.getClassLoader());
        in.readList(this.categories,String.class.getClassLoader());
        this.thumbnail = in.readString();

        this.year = in.readInt();
        this.brand = in.readString();
    }
}
