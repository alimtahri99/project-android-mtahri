package com.example.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerCollectionAdapter extends RecyclerView.Adapter<RecyclerCollectionAdapter.ViewHolderChild> {
    private List<ArticleAdapter> articleAdapterList;
    private static ClickListenerChild clickListenerCollection;

    public RecyclerCollectionAdapter(List<ArticleAdapter> articleAdapterList) {
        this.articleAdapterList = articleAdapterList;
    }

    @NonNull
    @Override
    public ViewHolderChild onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.collection_item,parent,false);
        return new ViewHolderChild(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderChild holder, int position) {
        holder.bind(articleAdapterList.get(position));
    }

    @Override
    public int getItemCount() {
        return articleAdapterList.size();
    }

    public ArticleAdapter getArtefactAtPosition(int position){
        return articleAdapterList.get(position);
    }

    public interface ClickListenerChild{
        void onItemClickChild(ArticleAdapter articleAdapter, View v);
    }
    void setClickListenerSection(ClickListenerChild clickListenerCollection){
        RecyclerCollectionAdapter.clickListenerCollection =clickListenerCollection;
    }

    class ViewHolderChild extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView articleNameHolder;
        private ImageView articleImageHolder;


        ViewHolderChild(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            articleNameHolder = itemView.findViewById(R.id.articleNameItem);

            articleImageHolder = itemView.findViewById(R.id.articleImageItem);

        }

        void bind(ArticleAdapter item) {
            articleNameHolder.setText(item.getName());
            if (item.getThumbnail() != null && !item.getThumbnail().isEmpty()) {
                Picasso.with(itemView.getContext()).load(item.getThumbnail()).into(articleImageHolder);
            } else {
                articleImageHolder.setImageResource(R.mipmap.ic_launcher);
            }
        }

        @Override
        public void onClick(View v) {
            clickListenerCollection.onItemClickChild(getArtefactAtPosition(getAdapterPosition()),v);
        }
    }
}
