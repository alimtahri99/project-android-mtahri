package com.example.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ArticleRecyclerAdapter extends RecyclerView.Adapter<ArticleRecyclerAdapter.ArticleViewHolder> implements Filterable {

    private static List<ArticleAdapter> articleAdapterListAdapter;
    private static List<ArticleAdapter> articleAdapterListAdapterAll;
    private Context context;
    private static ClickListener clickListener;

    public ArticleRecyclerAdapter(Context context, List<ArticleAdapter> articleAdapterListAdapter) {
        this.context=context;
        this.articleAdapterListAdapter = articleAdapterListAdapter;
        this.articleAdapterListAdapterAll = articleAdapterListAdapter;
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.collection_item, parent, false);
        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {
        holder.bind(articleAdapterListAdapter.get(position));
    }

    @Override
    public int getItemCount() {
        return articleAdapterListAdapter.size();
    }

    public static ArticleAdapter getArticleInPosition(int position){
        return articleAdapterListAdapter.get(position);
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    articleAdapterListAdapter = articleAdapterListAdapterAll;
                } else {
                    List<ArticleAdapter> filteredList = new ArrayList<>();
                    for (ArticleAdapter articleAdapter : articleAdapterListAdapterAll) {

                        if (articleAdapter.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(articleAdapter);
                        }else {
                            for (int i = 0; i < articleAdapter.getCategories().size(); i++) {
                                if(articleAdapter.getCategories().get(i).contains(charString.toLowerCase())){
                                    filteredList.add(articleAdapter);
                                }
                            }
                        }
                    }
                    articleAdapterListAdapter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = articleAdapterListAdapter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                    articleAdapterListAdapter = (List<ArticleAdapter>) results.values;
                    notifyDataSetChanged();
            }
        };
    }

    public interface ClickListener{
        void onItemClick(int position,View v);
    }
    public void setOnItemClicklistener(ClickListener clicklistener){

        clickListener=clicklistener;
    }

    static class ArticleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView articleNameHolder;

        private ImageView articleImageHolder;


        ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            articleNameHolder = itemView.findViewById(R.id.articleNameItem);

            articleImageHolder = itemView.findViewById(R.id.articleImageItem);

        }

        void bind(ArticleAdapter item) {
            articleNameHolder.setText(item.getName());


            if (item.getThumbnail() != null && !item.getThumbnail().isEmpty()) {
                Picasso.with(itemView.getContext()).load(item.getThumbnail()).into(articleImageHolder);
            } else {
                articleImageHolder.setImageResource(R.mipmap.ic_launcher);
            }
        }

        @Override
        public void onClick(View v) {


            clickListener.onItemClick(getAdapterPosition(),v);
        }
    }
}
