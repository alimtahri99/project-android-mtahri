package com.example.Url;

import android.util.JsonReader;

import com.example.Adapter.ArticleAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JSONResponseHandlerArticle {
    private static final String TAG = JSONResponseHandlerArticle.class.getSimpleName();
    private List<ArticleAdapter> articleAdapterList;

    public JSONResponseHandlerArticle(List<ArticleAdapter> articleAdapterList) {
        this.articleAdapterList = articleAdapterList;
    }


    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCatalog(reader);
        } finally {
            reader.close();
        }

    }
    private void readCatalog(JsonReader reader) throws IOException {
        reader.beginObject();
        while(reader.hasNext()){
            ArticleAdapter articleAdapter =new ArticleAdapter(reader.nextName());
            reader.beginObject();
            while (reader.hasNext()){
                String name=reader.nextName();
                switch (name){
                    case "name":
                        articleAdapter.setName(reader.nextString());
                        break;

                    case "description":
                        articleAdapter.setDescription(reader.nextString());
                        break;


                    case "year":
                        articleAdapter.setYear(reader.nextInt());
                        break;
                    case "brand":
                        articleAdapter.setBrand(reader.nextString());
                        break;

                        case "technicalDetails":
                        reader.beginArray();
                        while (reader.hasNext()){
                            reader.skipValue();
                        }
                        reader.endArray();
                        break;

                    case "pictures":
                        reader.beginObject();
                        while (reader.hasNext()){
                            reader.skipValue();
                        }
                        reader.endObject();
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            articleAdapter.setThumbnail(Web.GetThumbnailById(articleAdapter.getId()).toString());
            articleAdapterList.add(articleAdapter);
            reader.endObject();
        }
    }
}
