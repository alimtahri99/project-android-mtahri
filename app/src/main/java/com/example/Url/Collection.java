package com.example.Url;

import com.example.Adapter.ArticleAdapter;

import java.util.ArrayList;
import java.util.List;

public class Collection {
    private String collectionName;
    private List<ArticleAdapter> articleAdapterListSection =new ArrayList<>();

    public Collection(String sectionName) {
        this.collectionName = sectionName;
    }
    public String getcollectionName() {

        return collectionName;
    }
    public void setcollectionName(String sectionName) {

        this.collectionName = collectionName;
    }
    public List<ArticleAdapter> getArticleAdapterListSection() {


        return articleAdapterListSection;
    }

    public void populateListSection(List<ArticleAdapter> articleAdapterListAll){
        for (int i = 0; i < articleAdapterListAll.size(); i++) {


            for (int j = 0; j < articleAdapterListAll.get(i).getCategories().size(); j++) {


                if(articleAdapterListAll.get(i).getCategories().get(j).equals(collectionName)){


                    articleAdapterListSection.add(articleAdapterListAll.get(i));
                }
            }
        }
    }
    public ArticleAdapter getArticlePosition(int position){
        return articleAdapterListSection.get(position);
    }
}
