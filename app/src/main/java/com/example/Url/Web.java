package com.example.Url;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class Web {

    private static final String BaseHost = "demo-lia.univ-avignon.fr";

    private static final String SubHost = "cerimuseum";
    
    
    private static final String SEARCH_CATALOG = "collection";
    
    
    
    private static final String SEARCH_CATEGORIES = "categories";
    
    private static final String SEARCH_THUMBNAIL = "thumbnail";
    
    
    private static final String SEARCH_ITEMS = "items";
    
    
    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(BaseHost)
                .appendPath(SubHost);
        return builder;
    }

    public static URL SearchCategories() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATEGORIES);
        URL url = new URL(builder.build().toString());
        return url;
    }

    public static URL SearchCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    public static URL GetThumbnailById(String idArt) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS).appendPath(idArt).appendPath(SEARCH_THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }

}
