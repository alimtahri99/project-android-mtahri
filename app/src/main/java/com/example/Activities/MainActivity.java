package com.example.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.example.Adapter.ArticleAdapter;
import com.example.Adapter.ArticleRecyclerAdapter;
import com.example.Url.Collection;
import com.example.R;
import com.example.Url.Web;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    public  static List<ArticleAdapter> articleAdapterList = new ArrayList<>();
    public static List<Collection> collectionList=new ArrayList<>();
    private RecyclerView recyclerView;
    public SwipeRefreshLayout RefreshLayout;
    private ArticleRecyclerAdapter articleRecyclerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar tool = findViewById(R.id.toolbar);
        setSupportActionBar(tool);
        RefreshLayout = findViewById(R.id.swipeRefresh);
        recyclerView=findViewById(R.id.articleListRecyclerView);

        if(articleAdapterList.size()==0){
            ArticleActivity articleActivity =new ArticleActivity(articleAdapterList,MainActivity.this);
            try {
                articleActivity.execute(Web.SearchCatalog());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        collectionList.clear();
        initSection();
        articleRecyclerAdapter =new ArticleRecyclerAdapter(this, articleAdapterList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(articleRecyclerAdapter);
        articleRecyclerAdapter.setOnItemClicklistener((position, v) -> {
            Intent intent= new Intent(MainActivity.this, ArticleDetailActivity.class);
            intent.putExtra(ArticleAdapter.TAG, ArticleRecyclerAdapter.getArticleInPosition(position));
            startActivity(intent);
        });

        RefreshLayout.setOnRefreshListener(()->{
            if(articleAdapterList.size()==0){
                initSection();
                ArticleActivity artActivity =new ArticleActivity(articleAdapterList,MainActivity.this);
                try {
                    artActivity.execute(Web.SearchCatalog());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            }
            else{
                RefreshLayout.setRefreshing(false);
            }
            collectionList.clear();
            initSection();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if(id== R.id.sortName){
            item.setChecked(!item.isChecked());
            Collections.sort(articleAdapterList,new ArticleCompare(ArticleCompare.CompareOption.NOM));
            updateRecycler();
            return true;
        }
        else if(id== R.id.sortYear){
            item.setChecked(!item.isChecked());
            Collections.sort(articleAdapterList,new ArticleCompare(ArticleCompare.CompareOption.YEAR));
            updateRecycler();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateRecycler(){
        recyclerView.setAdapter(articleRecyclerAdapter);
    }
    public void initSection() {
    }
    }

