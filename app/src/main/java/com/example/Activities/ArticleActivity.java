package com.example.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.example.Adapter.ArticleAdapter;
import com.example.Url.JSONResponseHandlerArticle;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressLint("StaticFieldLeak")
public class ArticleActivity extends AsyncTask<URL,Integer, ArrayList<ArticleAdapter>> {
    private Context context;

    private List<ArticleAdapter> articleAdapterList;

    public ArticleActivity(List<ArticleAdapter> articleAdapterList, Context context) {
        this.articleAdapterList = articleAdapterList;
        this.context=context;
    }
    @Override
    protected ArrayList<ArticleAdapter> doInBackground(URL... urls) {
        InputStream inputStream;
        JSONResponseHandlerArticle jsonResponseHandlerArticle =new JSONResponseHandlerArticle(this.articleAdapterList);
        HttpURLConnection httpURLConnection;

        try {
            httpURLConnection=(HttpURLConnection) urls[0].openConnection();
            inputStream=new BufferedInputStream(httpURLConnection.getInputStream());
            try{
                jsonResponseHandlerArticle.readJsonStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (ArrayList<ArticleAdapter>) this.articleAdapterList;
    }

    @Override
    protected void onPostExecute(ArrayList<ArticleAdapter> articleAdapters) {
        super.onPostExecute(articleAdapters);
        if(context instanceof MainActivity){
            ((MainActivity) context).updateRecycler();
            ((MainActivity) context).RefreshLayout.setRefreshing(false);
            Collections.sort(articleAdapterList,new ArticleCompare(ArticleCompare.CompareOption.NOM));
        }
    }
}
