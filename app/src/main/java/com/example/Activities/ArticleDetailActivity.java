package com.example.Activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.Adapter.ArticleAdapter;
import com.example.R;
import com.squareup.picasso.Picasso;

public class ArticleDetailActivity extends AppCompatActivity {
    private static final String TAG = ArticleDetailActivity.class.getSimpleName();
    private ArticleAdapter articleAdapter;

    private TextView textArticleName, textArticleBrand ,textArticleDescription ;
    private ImageView ArticleThumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        articleAdapter = getIntent().getParcelableExtra(ArticleAdapter.TAG);
        textArticleName =findViewById(R.id.articleNameDetail);
        textArticleBrand =findViewById(R.id.brandView);
        textArticleDescription =findViewById(R.id.descriptionView);
        ArticleThumbnail =findViewById(R.id.articleThumbnailDetail);
        updateView();
    }

    @SuppressLint("SetTextI18n")
    void updateView(){
        textArticleName.setText(articleAdapter.getName());
        if(articleAdapter.getBrand()!=null){
            textArticleBrand.setText(articleAdapter.getBrand());
        }
        textArticleDescription.setText(articleAdapter.getDescription());

        if (articleAdapter.getThumbnail() != null && !articleAdapter.getThumbnail().isEmpty()) {
            Picasso.with(this).load(articleAdapter.getThumbnail()).into(ArticleThumbnail);
        } else {
            ArticleThumbnail.setImageResource(R.mipmap.ic_launcher);
        }
    }
}
