package com.example.Activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.Url.Collection;
import com.example.R;

import java.util.List;

public class CollectionActivity extends AppCompatActivity {
    private RecyclerView recyclerViewCollection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);
        recyclerViewCollection = findViewById(R.id.recyclerViewCollection);
        recyclerViewCollection.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerViewCollection.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));


    }
}
