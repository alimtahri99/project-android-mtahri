package com.example.Activities;

import com.example.Adapter.ArticleAdapter;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class ArticleCompare implements Comparator<ArticleAdapter> {


    private static Collator COLLATOR = Collator.getInstance(Locale.FRENCH);


    private CompareOption optionComp;

    public ArticleCompare(CompareOption optionComparator)
    {
        this.optionComp = optionComparator;
    }

    @Override
    public int compare(ArticleAdapter a, ArticleAdapter b) {
        if(optionComp== CompareOption.NOM){
            return COLLATOR.compare(a.getName(),b.getName());
        }
        else{
            int result=a.getYear()-b.getYear();
            if(result==0){
                return COLLATOR.compare(a.getName(),b.getName());
            }
            else {
                return result;
            }
        }
    }
    public enum CompareOption{
        YEAR,NOM;
    }
}
