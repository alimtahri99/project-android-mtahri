package com.example.Activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.R;
import com.google.android.material.snackbar.Snackbar;


public class ActivityRun extends AppCompatActivity {
    private long time=4000 ,ms=0 ;
    private boolean sActivityRun=true, paused=false;




    private void startMainActivity() {
        Intent i=new Intent(ActivityRun.this,MainActivity.class);
        startActivity(i);
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen);

        final ConstraintLayout cl=findViewById(R.id.splachlayout);
        Thread thread=new Thread(){
          public void run(){ try{
                while(sActivityRun && ms<time){
                    if(!paused){
                        ms=ms+100;
                    }
                    sleep(100);
                }
              }catch (Exception ignored){
              }finally {
                  if(!isOnline()){
                      Snackbar snackbar=Snackbar
                              .make(cl,"Connexion Problem",Snackbar.LENGTH_INDEFINITE)
                              .setAction("Réessayer", v -> recreate());
                      snackbar.show();
                  }else {
                      startMainActivity();
                  }
              }
          }
        };
        thread.start();
    }

    private boolean isOnline(){
        ConnectivityManager connectivityManager= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

}
